package org.keremulutas.mockeyjockey.core.generator;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.keremulutas.mockeyjockey.MockeyJockey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SelectionGeneratorTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SelectionGeneratorTest.class);

    private static MockeyJockey mj;

    @BeforeAll
    static void setup() {
        // log.info("@BeforeAll - executes once before all test methods in this class");
        mj = new MockeyJockey();
    }

    @DisplayName("For N elements specified with .withElements call, Random Selection returns all items given exactly once for a list of N elements")
    @Test
    void testAllItemsAreSelectedExactlyOnceWithCircularSelectionEnabled() {
        int count = 1_000;

        List<Integer> integerList = IntStream.range(0, count)
            .boxed()
            .collect(Collectors.toList());

        SelectionGenerator.Randomized<Integer> randomSelectionGeneratorWithElements = mj.randomSelection(Integer.class)
            .withElements(integerList)
            .isCircular(true);

        assertTrue(randomSelectionGeneratorWithElements._isCircular, "Circular selection should be enabled");
        assertEquals(randomSelectionGeneratorWithElements._objectClass, Integer.class, "Object class should be Integer class");

        List<Integer> generatedList1_1 = randomSelectionGeneratorWithElements.list(count).get();

        assertEquals(generatedList1_1.size(), count, "Generated list size is not as expected");
        assertTrue(generatedList1_1.containsAll(integerList), "Generated list contains all elements specified in withElements method call");

        List<Integer> generatedList1_2 = randomSelectionGeneratorWithElements.list(count).get();

        assertEquals(generatedList1_2.size(), count, "Generated list 2nd try size is not as expected");
        assertTrue(generatedList1_2.containsAll(integerList), "Generated list 2nd try contains all elements specified in withElements method call");

        SelectionGenerator.Randomized<Integer> randomSelectionGeneratorWithSourceGenerator = mj.randomSelection(Integer.class)
            .source(mj.constant(integerList))
            .isCircular(true);

        assertTrue(randomSelectionGeneratorWithSourceGenerator._isCircular, "Circular selection should be enabled");
        assertEquals(randomSelectionGeneratorWithSourceGenerator._objectClass, Integer.class, "Object class should be Integer class");

        List<Integer> generatedList2 = randomSelectionGeneratorWithSourceGenerator.list(count).get();

        assertEquals(generatedList2.size(), count, "Generated list size is not as expected");
        assertTrue(generatedList2.containsAll(integerList), "Generated list contains all elements returned by generator specified in source method call");
    }

}
