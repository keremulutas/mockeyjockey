package org.keremulutas.mockeyjockey.core.generator;

import org.keremulutas.mockeyjockey.core.exception.MockeyJockeyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Supplier;

public abstract class SelectionGenerator<T> extends Generator<Void, T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SelectionGenerator.class);

    Supplier<List<T>> _sourceGenerator;
    List<T> _currentList;
    boolean _isCircular = false;
    final Class<T> _objectClass;

    SelectionGenerator(Class<T> clz, Random randomizer) {
        super(randomizer);
        this._objectClass = clz;
    }

    public SelectionGenerator<T> isCircular(boolean isCircular) {
        this._isCircular = isCircular;
        return this;
    }

    public SelectionGenerator<T> source(Supplier<List<T>> sourceGenerator) {
        this._sourceGenerator = sourceGenerator;
        return this;
    }

    public SelectionGenerator<T> withElements(T... elements) {
        return this.withElements(Arrays.asList(elements));
    }

    public SelectionGenerator<T> withElements(Collection<T> elements) {
        if(elements.isEmpty()) {
            throw new MockeyJockeyException(".withElements called with empty collection", this.getClass().getName(), this._tag);
        }
        ArrayList<T> list = new ArrayList<>(elements);
        this._sourceGenerator = new ConstantGenerator<>(list, this._randomizer);
        this._isCircular = true;
        return this;
    }

    public static class Randomized<T> extends SelectionGenerator<T> {

        public Randomized(Class<T> clz, Random randomizer) {
            super(clz, randomizer);
        }

        @Override
        public SelectionGenerator.Randomized<T> isCircular(boolean isCircular) {
            return (SelectionGenerator.Randomized<T>) super.isCircular(isCircular);
        }

        @Override
        public SelectionGenerator.Randomized<T> source(Supplier<List<T>> sourceGenerator) {
            return (SelectionGenerator.Randomized<T>) super.source(sourceGenerator);
        }

        @Override
        public SelectionGenerator.Randomized<T> withElements(T... elements) {
            return (SelectionGenerator.Randomized<T>) super.withElements(elements);
        }

        @Override
        public SelectionGenerator.Randomized<T> withElements(Collection<T> elements) {
            return (SelectionGenerator.Randomized<T>) super.withElements(elements);
        }

        // https://stackoverflow.com/a/51618656/322709
        private Iterator<T> getIterator() {
            return new Iterator<T>() {
                int i = 0;
                final int n = _currentList.size();
                final HashMap<Integer, T> shuffled = new HashMap<>();

                public boolean hasNext() { return i < n; }

                public T next() {
                    int j = i+_randomizer.nextInt(n-i);
                    T a = get(i), b = get(j);
                    shuffled.put(j, a);
                    shuffled.remove(i);
                    ++i;
                    return b;
                }

                T get(int i) {
                    return shuffled.containsKey(i) ? shuffled.get(i) : _currentList.get(i);
                }
            };
        }

        private Iterator<T> _currentIterator;

        @Override
        protected T generate() {
            if (this._sourceGenerator == null) {
                throw new MockeyJockeyException("Source generator must be supplied", this.getClass().getName(), this._tag);
            }

            if (this._currentList == null || this._currentList.size() == 0) {
                this._currentList = new ArrayList<>(this._sourceGenerator.get());
            }

            T value;

            int index = this._randomizer.nextInt(this._currentList.size());
            if (this._isCircular) {
                if(this._currentIterator == null || ! this._currentIterator.hasNext()) {
                    this._currentIterator = this.getIterator();
                }

                value = this._currentIterator.next();
            } else {
                value = this._currentList.get(index);
            }

            return value;
        }

    }

    public static class Sequential<T> extends SelectionGenerator<T> {

        private int _currentIndex = 0;

        public Sequential(Class<T> clz, Random randomizer) {
            super(clz, randomizer);
        }

        @Override
        public SelectionGenerator.Sequential<T> isCircular(boolean isCircular) {
            return (SelectionGenerator.Sequential<T>) super.isCircular(isCircular);
        }

        @Override
        public SelectionGenerator.Sequential<T> source(Supplier<List<T>> sourceGenerator) {
            return (SelectionGenerator.Sequential<T>) super.source(sourceGenerator);
        }

        @Override
        public SelectionGenerator.Sequential<T> withElements(T... elements) {
            return (SelectionGenerator.Sequential<T>) super.withElements(elements);
        }

        @Override
        public SelectionGenerator.Sequential<T> withElements(Collection<T> elements) {
            return (SelectionGenerator.Sequential<T>) super.withElements(elements);
        }

        @Override
        protected T generate() {
            if (this._sourceGenerator == null) {
                throw new MockeyJockeyException("Source generator or elements must be supplied", this.getClass().getName(), this._tag);
            }

            if (this._currentList == null || this._currentList.size() == 0 || ( !this._isCircular && this._currentIndex % this._currentList.size() == 0 )) {
                this._currentList = this._sourceGenerator.get();
                if (this._currentList.size() == 0) {
                    throw new MockeyJockeyException("Source generator generated a list with 0 elements", this.getClass().getName(), this._tag);
                }
            }

            T value = this._currentList.get(this._currentIndex % this._currentList.size());
            this._currentIndex++;

            return value;
        }

    }

    @Override
    public Class<T> getTargetObjectClass() {
        return this._objectClass;
    }

}
